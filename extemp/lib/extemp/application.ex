defmodule Extemp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  require RingLogger
  require Logger

  @target Mix.Project.config()[:target]

  use Application

  def start(_type, _args) do
    opts = [strategy: :one_for_one, name: Extemp.Supervisor]
    Supervisor.start_link(children(@target), opts)
  end

  # List all child processes to be supervised
  def children("host") do
    [
      {Extemp.Server, []}
      # {Coap.Storage, [[], []]}
    ]
  end

  def children(_target) do
    [
      {Extemp.Server, []}
      #   {Coap.Storage, [[], []]},
      #   {:coap_server, []}
    ]
  end
end
