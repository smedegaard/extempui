defmodule Extemp.Server do
  use GenServer
  require Logger
  import ExtempSensors

  @base_dir "/sys/bus/w1/devices/"
  @interval 2000

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init(_opts) do
    RingLogger.attach()

    sensors =
      File.ls!(@base_dir)
      |> get_sensors()

    send(self(), :read_temp)

    {:ok,
     %{
       sensors: sensors,
       temperatures: %{}
     }}
  end

  def handle_info(:read_temp, state) do
    temps =
      state[:sensors]
      |> Enum.with_index()
      |> Enum.map(&read_temp(&1, File.read!("#{base_dir}#{sensor}/w1_slave")))

    state =
      %{state | temperatures: temps}
      |> IO.inspect()

    Process.send_after(self(), :read_temp, @interval)
    {:noreply, state}
  end

  def handle_call(:get_temps, _from, state) do
    {:reply, state[:temperatures], state}
  end

  ## PUBLIC API

  def get_temps() do
    GenServer.call(__MODULE__, :get_temps)
  end
end
