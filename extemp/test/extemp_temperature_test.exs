defmodule ExtempTemperatureTest do
  use ExUnit.Case
  doctest Extemp.Temperature

  alias Extemp.Temperature

  @sensors ["28-sensor1", "random-file.txt", "28-sensor2"]

  test "get_sensors returns a list of sensor names" do
    assert Temperature.get_sensors(@sensors) == ["28-sensor1", "28-sensor2"]
  end
end
