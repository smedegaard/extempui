defmodule ExtempSensors do
  @moduledoc """
  A module for to working with sensors to make great benefit for developer.
  GREAT SUCCESS!
  """
  require Logger

  @spec get_sensors(list(String.t())) :: list()
  @doc """
  Given a list of file names, it returns a filtered list of file names that starts with '28-'

  ## Examples

    iex> ExtempSensors.get_sensors(["28-sensor1", "random.txt", "28-sensor2.ext"])
    ["28-sensor1", "28-sensor2.ext"]

  """
  def get_sensors(files) do
    Enum.filter(files, &String.starts_with?(&1, "28-"))
  end

  @spec read_temp({any, integer}, binary) :: %{key: number}
  @doc """
  Reads the temperature of a sensor.
  Expects `{_sensor, index}, sensor_data` as input

  returns a `%{sensor_name: value}` map

  `sensor_name` is based on the `index`

  ## Examples
  
      iex> ExtempSensors.read_temp({:sensor_name, 0}, "foo bar baz t=-273150")
      %{sensor_0: -273.15}
  """
  def read_temp({_sensor, index}, sensor_data) do
    # sensor_data = File.read!("#{base_dir}#{sensor}/w1_slave")
    #Logger.debug("reading sensor: #{sensor}: #{sensor_data}")
    {temp, _} =
      Regex.run(~r/(?<=t=)(-?)(\d+)/, sensor_data)
      |> List.first()
      |> Float.parse()

    sensor_name =
      ("sensor_" <> Integer.to_string(index))
      |> String.to_atom()

    %{sensor_name => temp / 1000}
  end
end
