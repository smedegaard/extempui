defmodule ExtempSensorsTest do
  use ExUnit.Case
  doctest ExtempSensors

  @files ["28-sensor1", "random.txt", "28-sensor2.ext"]
  @sensor_content_neg "lkasdmlaksdmalkdm t=-273150 asdasdasd"
  @sensor_content_pos "foo bar baz t=24255 asldkmlk"

  test "get_sensors() returns a filtered list of file names" do
    assert ExtempSensors.get_sensors(@files) == ["28-sensor1", "28-sensor2.ext"]
  end

  test "given an empty list get_sensors() returns an empty list" do
    assert ExtempSensors.get_sensors([]) == []
  end

  test "given a binary with valid positive content, read_temp returns a map" do
    assert ExtempSensors.read_temp({:sensor_name, 0}, @sensor_content_neg) == %{
             :sensor_0 => -273.15
           }
  end

  test "given a binreary with valid negative, read_temp returns a map" do
    assert ExtempSensors.read_temp({:sensor_name, 0}, @sensor_content_pos) == %{
             :sensor_0 => 24.255
           }
  end
end
