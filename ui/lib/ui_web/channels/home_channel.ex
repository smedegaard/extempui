defmodule UiWeb.HomeChannel do
  use UiWeb, :channel
  require Logger

  def join("home:lobby", payload, socket) do
    if authorized?(payload) do
      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (home:lobby).
  def handle_in("shout", payload, socket) do
    broadcast(socket, "shout", payload)
    {:noreply, socket}
  end

  def handle_in("update_temps", _payload, socket) do
    broadcast!(socket, "temp_updated", generate_fake_temps())
    {:noreply, socket}
  end

  def handle_in("temp_updated", %{"body" => body}, socket) do
    broadcast!(socket, "temp_updated", %{body: body})
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end

  defp generate_fake_temps() do
    %{
      "body" => [
        %{sensor_name: :sensor_0, temp: Enum.random(-100..100)},
        %{sensor_name: :sensor_1, temp: Enum.random(-100..100)}
      ]
    }
  end
end
